package pl.pc;

/**
 * Created by pawelcala on 6/8/16.
 */
public class Payload {

    String url;
    String title;
    String body;

    public Payload(String url, String title, String body) {
        this.url = url;
        this.title = title;
        this.body = body;
    }
}
